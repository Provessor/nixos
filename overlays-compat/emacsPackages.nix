{ pkgs ? import <nixpkgs> {} }:

let
  emacs = pkgs.emacsHEAD;
  emacsWithPackages = (pkgs.emacsPackagesGen emacs).emacsWithPackages;
in
  emacsWithPackages (epkgs: (with epkgs.melpaStablePackages; [
    use-package
    spacemacs-theme
    which-key
    popup-kill-ring
    sudo-edit
    ido-vertical-mode
    smex
    expand-region
    swiper
    multiple-cursors
    ace-jump-mode
    switch-window
    magit
    evil-numbers
    yasnippet
    company
    nix-mode
  ]) ++ [
    pkgs.auctex
  ])