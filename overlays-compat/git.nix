self: super: {
  git = (super.git.override {
    perlSupport = false;
    pythonSupport = false;
    withpcre2 = false;
  });
}