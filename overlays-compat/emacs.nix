self: super: {
  emacsHEAD = (super.emacs.override {
    srcRepo = true;
  }).overrideAttrs (old: rec {
    name = "emacs-${version}${versionModifier}";
    version = "27.0";
    versionModifier = ".50";

    src = self.fetchgit {
      url = "https://git.savannah.gnu.org/git/emacs.git";
      rev = "f113ae59222dbb0961a34c1e0913cf0f3104a567";
      sha256 = "02sbgy4yaa78729p6lry2xpwymnp4nv43r5y8kxq48w98avw797a";
    };

    patches = [ ];
  });

  emacsWithPackages = (self.emacsPackagesGen self.emacsHEAD).emacsWithPackages;
}