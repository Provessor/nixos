# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, options, ... }:

{
  imports = [ ./hardware-configuration.nix ];

  nix.nixPath = options.nix.nixPath.default ++
                [ "nixpkgs-overlays=/etc/nixos/overlays-compat/" ];
  nixpkgs.overlays = [
    (import ./overlays-compat/emacs.nix)
    (import ./overlays-compat/mpv.nix)
    # (import ./overlays-compat/git.nix)
  ];

  # Use the systemd-boot EFI boot loader.
  boot = {
    tmpOnTmpfs = true;
    # plymouth.enable = true;

    loader.grub = {
      enable = true;
      version = 2;
      device = "/dev/sdb";
      useOSProber = true;
    };
  };

  networking.hostName = "gib";

  i18n = {
    consoleFont = "Lat2-Terminus16";
    consoleKeyMap = "us";
    defaultLocale = "en_AU.utf8";
  };

  time.timeZone = "Australia/Brisbane";

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    (import ./overlays-compat/emacsPackages.nix { inherit pkgs; })

    busybox
    mg
    ntfs3g
    firefox
    resilio-sync
    git
    xclip
    clang
    clang-tools
    ccls
    gnupg
    stow
    mpv-with-scripts
    keepassxc
    zotero
    youtube-dl
    xorg.xdpyinfo
    # qemu
    hunspell
    hunspellDicts.en-au-large
    freerdp
    gimp
    riot-desktop
    #canon-cups-ufr2
    #cnijfilter2
    (texlive.combine {
      inherit (texlive) scheme-basic
        collection-publishers
        collection-latexrecommended
        collection-mathscience
        chktex;
    })

    # Ubuntu MATE
    gnome3.evolution
  ];

  fonts.fonts = with pkgs; [
    meslo-lg
    emacs-all-the-icons-fonts
    font-awesome_4
    ibm-plex
  ];

  programs = {
    zsh = {
      enable = true;
      enableCompletion = true;
      autosuggestions.enable = true;
      syntaxHighlighting.enable = true;
      ohMyZsh = {
        enable = true;
        plugins = [ "git" "man" ];
        theme = "gentoo";
      };
    };
    gnupg.agent = { enable = true; enableSSHSupport = true; };
  };

  networking.firewall = {
    enable = true;
    # allowedTCPPorts = [  ];
    # allowedUDPPorts = [  ];
  };

  sound.enable = true;
  hardware.pulseaudio.enable = true;

  services = {
    printing.enable = false;
    openssh.enable = true;
    cron.enable = true;
    xserver = {
      enable = true;
      videoDrivers = [ "radeon" ];
      deviceSection = ''
        Option "TearFree" "on"
      '';
      layout = "us";
      xkbOptions = "ctrl:swapcaps";
      libinput.enable = true;
      desktopManager = {
        default = "mate";
        xterm.enable = false;
        mate.enable = true;
      };
      displayManager.auto = {
        enable = true;
        user = "prov";
        # greeters.gtk.indicators = [ "~host" "~spacer" "~clock" "~spacer" "~session" "~language" "~a11y" "~power" ];
      };
    };
  };

  users = {
    mutableUsers = false;
    users.prov = {
      isNormalUser = true;
      description = "Jai Flack";
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [ "AAAAC3NzaC1lZDI1NTE5AAAAINoFNTh5qVy3iQOTTU4ZoXHVAbLQ6pUGMInNW+7QSlFS" ];
      shell = pkgs.zsh;
      hashedPassword = "$6$rBnWyNnwp$oBfxVF4zhb7d47Z6WirkXVY0xkHL6KcByN1Msp3tt6mqHP8dtsZjJQUVCuYpHp7QGnE2wyO/DkJeJGFZaVD0h1";
    };
  };

  system.stateVersion = "19.03"; # Did you read the comment?
}